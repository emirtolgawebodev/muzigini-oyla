﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MuzigiOyla3.Models;

namespace MuzigiOyla3.Controllers
{
    public class AlbumController : Controller
    {
        private MuzigiOyla2Entities db = new MuzigiOyla2Entities();
        // GET: Album
        public ActionResult Index()
        {
            ViewBag.albums = db.Albums.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Detail(int id)
        {
            var album = db.Albums.Find(id);
            ViewBag.album = album;
            var review = new Review()
            {
                AlbumId = album.Id
            };
            return View("Detail",review);
        }
        
        [HttpPost]
        public ActionResult SendReview(Review review)
        {
            string username = Session["username"].ToString();
            review.DatePost = DateTime.Now;
            review.AccountId = db.Accounts.Single(a => a.Username.Equals(username)).Id;
            db.Reviews.Add(review);
            db.SaveChanges();
            return RedirectToAction("Detail", "Album", new { id = review.AlbumId });
        }
        
    }
}