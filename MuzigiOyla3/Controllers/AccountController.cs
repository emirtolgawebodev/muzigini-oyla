﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MuzigiOyla3.Models;

namespace MuzigiOyla3.Controllers
{
    public class AccountController : Controller
    {
        private MuzigiOyla2Entities db = new MuzigiOyla2Entities();
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            return View("Login");
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var count = db.Accounts.Count(a => a.Username.Equals(username) && a.Password.Equals(password));
            if (count > 0)
            {
                Session["username"] = username;
                return RedirectToAction("Index","Album");
            }
            else
            {
                ViewBag.error = "Invalid Account";
                return View("Login");
            }
            return View();
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            return View("SignUp");
        }
        [HttpPost]
        public ActionResult SignUp(Account account)
        {
            db.Accounts.Add(account);
            db.SaveChanges();
            return RedirectToAction("Login", "Account");
        }

    }
}